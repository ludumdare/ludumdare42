
#	Copyright 2018, SpockerDotNet LLC

extends Control


var value

func _ready():
	value = find_node("Value")
	Signals.connect(Signals.GAME_HULL_CHANGED, self, "_on_hull_changed")
	
	
func _on_hull_changed(hull):
		value.text = "%02d" % hull
