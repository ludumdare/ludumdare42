
#	Copyright 2018, SpockerDotNet LLC

#	Gui for Ship Systems


extends Control


var warp_value
var speed_value
var shield_value
var weapon_value
var power_value

var warp_location
var speed_location
var shield_location
var weapon_location
var power_location

var value_format = "%d"

func _ready():
	
	warp_value = get_node("Container/Systems/Warp/Value")
	speed_value = get_node("Container/Systems/Speed/Value")
	shield_value = get_node("Container/Systems/Shields/Value")
	weapon_value = get_node("Container/Systems/Weapons/Value")
	power_value = get_node("Container/Systems/Power/Value")
	
	warp_location = get_node("Container/Warp")
	speed_location = get_node("Container/Speed")
	shield_location = get_node("Container/Shield")
	weapon_location = get_node("Container/Weapon")
	power_location = get_node("Container/Power")
	
	Signals.connect(Signals.GAME_WARP_CHANGED, self, "_on_warp_changed")
	Signals.connect(Signals.GAME_SPEED_CHANGED, self, "_on_speed_changed")
	Signals.connect(Signals.GAME_SHIELD_CHANGED, self, "_on_shield_changed")
	Signals.connect(Signals.GAME_WEAPON_CHANGED, self, "_on_weapon_changed")
	Signals.connect(Signals.GAME_POWER_CHANGED, self, "_on_power_changed")
	
	
func _on_warp_changed(value):
	warp_value.text = value_format % value
	
	
func _on_speed_changed(value):
	speed_value.text = value_format % value
	
	
func _on_shield_changed(value):
	shield_value.text = value_format % value
	
	
func _on_weapon_changed(value):
	weapon_value.text = value_format % value
	
	
func _on_power_changed(value):
	power_value.text = value_format % value
	
	
	