extends Node2D

var player

func _ready():
	player = get_node("PlayerIcon")
	Signals.connect(Signals.GAME_UPDATE_TRAVEL, self, "_on_update_travel")

func _on_update_travel(travel, travel_distance):
	var distance = travel * (400.0/travel_distance)
	player.position = Vector2(distance - 200, 0)
	