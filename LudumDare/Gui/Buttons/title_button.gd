extends Button

signal load_scene

export(String, FILE, "*.tscn") var scene

func _on_Button_pressed():
	print(scene)
	emit_signal("load_scene", scene)
	Signals.emit_signal(Signals.LOAD_SCENE, scene)