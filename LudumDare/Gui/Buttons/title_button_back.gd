extends Button

export(String, FILE, "*.tscn") var scene

func _on_Button_pressed():
	get_tree().change_scene(scene)
