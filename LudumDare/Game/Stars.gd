
#	Copyright 2018, SpockerDotNet LLC

#
#	A Starfield Emmiter

extends Node2D


var star_images = Array()
var ship_speed = 100
var star_timer

onready var star = preload("res://LudumDare/Game/Star.tscn")


func _ready():
	
	var star1 = load("res://LudumDare/Art/star1.png")	
	var star2 = load("res://LudumDare/Art/star2.png")	
	var star3 = load("res://LudumDare/Art/star3.png")
	
	star_images.append(star1)
	star_images.append(star2)
	star_images.append(star3)
	
	Signals.connect(Signals.GAME_SPEED_CHANGED, self, "_on_speed_changed")
	
	star_timer = get_node("Timer")


func _on_Timer_timeout():
	var new_star = star.instance()
	new_star.texture = star_images[rand_range(0, star_images.size())]
	new_star.set_speed(ship_speed)
	add_child(new_star)
	var y_offset = rand_range(-155, 155)
	new_star.position += Vector2(0, y_offset)
	
	
func _on_speed_changed(speed):
	ship_speed = speed
	var add = ((100 - speed)/100.0)
	star_timer.wait_time = 0.1 + add
	