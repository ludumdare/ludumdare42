extends Node2D

var target

func _ready():
	print(target)

func _process(delta):
	var direction = (target.global_position - global_position).normalized()
	var motion = direction * 100 * delta
	global_position += motion
	var distance = global_position.distance_to(target.global_position)
	if distance < 5:
		Signals.emit_signal(Signals.HIT_PLAYER, "missle", 10)
		free()
	
	