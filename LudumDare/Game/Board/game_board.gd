
#	Copyright 2018, SpockerDotNet LLC

extends Position2D


var columns = 12
var rows = 8
var pieces = {}
var selected


func can_swap(piece):
	if !selected:
		return false
		
	return _can_swap(piece)
	
	
func create():
	print("game_board:create")
	_create_pieces()
	_resolve_clusters()

func draw():
	print("game_board:draw")
	_draw_pieces()
	

func find_clusters():
	return _find_clusters()
	
	
func find_shifts():
	return _find_shifts()
	
	
func remove_clusters(clusters):
	_remove_clusters(clusters)
	
	
func shift_pieces():
	_shift_pieces()
	
	
func resolve_pieces():
	_resolve_pieces()
	
	
func swap(from_piece, to_piece):
	_swap(from_piece, to_piece)
	
	
func _can_swap(from_piece):
	
	var x1 = from_piece.board_location.x
	var y1 = from_piece.board_location.y
	var x2 = selected.board_location.x
	var y2 = selected.board_location.y
	
	if ((abs(x1-x2) == 1 && y1 == y2) || (abs(y1 - y2) == 1 && x1 == x2)):
		return true
		
	return false
	
	
func _create_pieces():
	print("game_board:_create_pieces")
	
	var root = get_node("Root")
	
	randomize()
	
	for x in range(columns):
		for y in range(rows):
			#print("{x}:{y}".format({ "x": x, "y": y}))
			var x_pos = x * 70
			var y_pos = y * 70
			var piece = load("res://LudumDare/Game/Pieces/Piece.tscn").instance()
			piece.name = piece.id
			piece.board_location = Vector2(x, y)
			piece.board_position = Vector2(x_pos, y_pos)
			piece.position = piece.board_position
			pieces[piece.board_location] = piece
			root.add_child(piece)

	
func _draw_pieces():
	print("game_board:_draw_pieces")
	
	for item in pieces:
		#print(item)
		pieces[item].draw()
	
	
func _find_clusters():
	print("game_board:_find_clusters")

	var clusters = Array()
	
	#	find horizontal clusters
	for row in range(rows):
		
		var match_length = 1
		var match_type = -1
		
		for col in range(columns):
			
			var check_cluster = false
			
			if col == columns - 1:
				check_cluster = true
			else:
				if pieces[Vector2(col, row)].type == pieces[Vector2(col + 1, row)].type && pieces[Vector2(col, row)].type != -1:
					match_length += 1
					match_type = pieces[Vector2(col, row)].type
				else:
					check_cluster = true
					
			if check_cluster:
				if match_length >= 3:
					clusters.append( { "board_location": Vector2(col + 1 - match_length, row), "length": match_length, "horizontal": true, "type": match_type })
			
				match_length = 1	
					
				
	#	find vertical clusters
	
	for col in range(columns):
		
		var match_length = 1
		var match_type = -1
		
		for row in range(rows):
			var check_cluster = false
			if row == rows - 1:
				check_cluster = true
			else:
				if pieces[Vector2(col, row)].type == pieces[Vector2(col, row + 1)].type && pieces[Vector2(col, row)].type != -1:
					match_length += 1
					match_type = pieces[Vector2(col, row)].type
				else:
					check_cluster = true
					
			if check_cluster:
				if match_length >= 3:
					clusters.append( { "board_location": Vector2(col, row + 1 - match_length), "length": match_length, "horizontal": false, "type": match_type })
			
				match_length = 1	
	
	return clusters


#	return list of pieces to shift down
func _find_shifts():
	print("game_board:_find_shifts")

	var shifts = Array()
	
	for x in range(columns):
		for y in range(rows - 2, -1, -1):
			var key = Vector2(x, y)
			var key_below = Vector2(x, y + 1)
			var piece = pieces[key]
			var piece_below = pieces[key_below]
			if piece.type != -1:
				if piece_below.type == -1:
					shifts.append({ "from": key, "to": key_below })
	
	return shifts
	

func _remove_cluster(cluster):
	print("game_board:_remove_cluster")
	print(cluster)
	
	var pos = cluster.board_location
	var x_offset = 0
	var y_offset = 0
	
	for l in range(cluster.length):
		var key = Vector2(pos.x + x_offset, pos.y + y_offset)
		var piece = pieces[key]
		piece.type = -1
		
		if cluster.horizontal:
			x_offset += 1
		else:
			y_offset += 1	
		
	
func _remove_clusters(clusters):
	print("game_board:_remove_clusters")
	
	for cluster in clusters:
		_remove_cluster(cluster)
			
	
func _resolve_clusters():
	print("game_board:_resolve_clusters")
	var clusters = _find_clusters()
	
	while clusters.size() > 0:
		
#		print(__dump__("type"))
		
		_remove_clusters(clusters)
		
		_shift_pieces()
		
		_resolve_pieces()
		
		clusters = _find_clusters()
		

func _resolve_pieces():
	print("game_board:_resolve_pieces")
	
	for piece in pieces.values():
		if piece.type == -1:
			piece.set_type()
			
		
func _shift_pieces():
	print("game_board:_shift_pieces")

	var shifts = _find_shifts()
	
	while shifts.size() > 0:
		
		for shift in shifts:
			print("from:%s, to:%s" % [shift.from, shift.to])
			_swap(pieces[shift.from], pieces[shift.to])
	
		shifts = _find_shifts()
		
			
func _swap(from_piece, to_piece):
#	print("game_board:_swap")
	
#	print(from_piece.__dump__())
#	print(to_piece.__dump__())
	
	
	
#	var id = from_piece.id
#	var board_location = from_piece.board_location
	var type1 = from_piece.type
	var type2 = to_piece.type
	from_piece.type = type2
	to_piece.type = type1
#
#	from_piece.id = to_piece.id
#	from_piece.board_location = to_piece.board_location
#	from_piece.board_position = Vector2(0, 0)
#	from_piece.type = to_piece.type
#	from_piece.is_selected = false
#	from_piece.is_dirty = true
#
#	to_piece.id = id
#	to_piece.board_location = board_location
#	to_piece.board_position = Vector2(0, 0)
#	to_piece.type = type
#	to_piece.is_selected = false
#	to_piece.is_dirty = true
	
#	print(from_piece.__dump__())
#	print(to_piece.__dump__())
	
#	var from_key = from_piece.board_location
#	var to_key = to_piece.board_location
#
#	var temp = from_piece
#
#	from_piece.board_location = Vector2(to_piece.board_location.x, to_piece.board_location.y)
#	to_piece.board_location = Vector2(temp.board_location.x, temp.board_location.y)
#
#	pieces[from_key] = to_piece
#	pieces[to_key] = temp
	
		
func _init():
	pass
	

func _ready():
	pass
	

func __dump__(report):

	var r = ""
	
	r += "dump %s\n" % report
	
	
	for y in range(rows):
		for x in range(columns):
			var v = Vector2(x, y)
			var p = pieces[v]
			var s = ""
			match report:
				"type":
					s = "(%3s)" % [ p.type ]
				"dirty":
					s = "(%5s)" % [ p.is_dirty ]
				"position":
					s = "(%5s)" % [ p.position ]
				"board_position":
					s = "(%5s)" % [ p.board_position ]
			r += s
		r += "\n"
		
	r += "\n"
	
	return r
