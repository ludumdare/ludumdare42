extends "res://LudumDare/Game/Ship.gd"

#	for whatever reason the _ready is not being called
#	so i am forcing the speed change in the Game code

var shield_ticks = 0
var show_shield = false

#	briefly display the shield
func show_shield():
	show_shield = true
	shield_ticks = 0

func _ready():
	print("player ship ready")
	Signals.connect(Signals.GAME_SPEED_CHANGED, self, "_on_speed_changed")
	
func _process(delta):
	
	if show_shield:
		get_node("Shield").show()
		
		shield_ticks += 1
		
		if shield_ticks > 240:				
			show_shield = false
			get_node("Shield").hide()
	

func _on_speed_changed(speed):
	print("speed changed")
	ship_speed = speed