
#	Copyright 2018, SpockerDotNet LLC

#	Moves a Star Across the Screen at a Random Speed
#	and then Kills itself after 10 seconds.

extends Sprite


var speed = 100


func _ready():
	pass	
	

func set_speed(new_speed):
	speed = 300 + (rand_range(5, 30) * new_speed)
	
	
func _process(delta):
	position += Vector2(-1, 0) * speed * delta	
	

func _on_Timer_timeout():
	queue_free()
