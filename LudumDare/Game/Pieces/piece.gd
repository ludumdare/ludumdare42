
#	Copyright 2018, SpockerDotNet LLC

extends Area2D


signal click_piece(piece)


enum piece_types { TYPE_WARP, TYPE_SPEED, TYPE_SHIELD, TYPE_WEAPON, TYPE_POWER }


var piece_resources = [
		"res://LudumDare/Art/Pieces/tileBlue_01.png",
		"res://LudumDare/Art/Pieces/tileGreen_04.png",
		"res://LudumDare/Art/Pieces/tileOrange_04.png",
		"res://LudumDare/Art/Pieces/tileYellow_07.png",
		"res://LudumDare/Art/Pieces/tileBlack_11.png",
	]
	

var id
var type
var board_location
var board_position
var is_selected
var is_dirty

func set_type():
	_set_type()
		

func draw():
	#print("piece:render")
	#print(type)
	
	var sprite = self.get_node("Sprite")
	sprite.texture = null
	
	if type != -1:
		show()
		var texture = load(piece_resources[type])
		sprite.texture = texture
	else:
		hide()
	
	var selected = self.get_node("Selected")
	if is_selected:
		selected.show()
	else:
		selected.hide()


#	return some piece settings
func reset():
	print("piece:reset")
#	print(board_position)
#	position = board_position
	is_selected = false
	is_dirty = false
	show()
	
	
#	simulate removing piece (global space)
func remove_piece(to_position):
	print("piece:remove_piece")
	
	#	create a new sprite
	var mock_sprite = _create_mock_sprite()
	hide()
		
	#	move the piece down slightly and then slowly fade out
	
	var tween = get_node("Tween")
	tween.interpolate_property(mock_sprite, "position", mock_sprite.position, to_position, 2.0, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.interpolate_property(mock_sprite, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1.9, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	mock_sprite.queue_free()
	
#	simulate moving a piece (global space)
func tween_to_position(to_position):
	print("piece:tween_to_position")
	
	#	create a new sprite
	var mock_sprite = _create_mock_sprite()
	hide()
	
	var tween = self.get_node("Tween")
	tween.interpolate_property(mock_sprite, "position", mock_sprite.position, to_position, 0.8, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()
	return { "tween": tween, "sprite": mock_sprite }
	

func move_to(to_position):
	#	create a new sprite
	var mock_sprite = _create_mock_sprite()
	
	var tween = get_node("Tween")
	tween.interpolate_property(mock_sprite, "position", mock_sprite.position, to_position, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	return { "tween": tween, "sprite": mock_sprite }

	
func _init():
	id = _get_unique_id(8)
	is_dirty = false
	is_selected = false
	set_type()
	
	
func _get_unique_id(level):
	var id_string ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	randomize()
	var id = ""
	for i in range(level):
		var pos = rand_range(0,id_string.length()-1)
		var next = id_string.substr(pos, 1)
		id += next
	return id


func _on_Piece_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.is_pressed():
				print(__dump__())
				emit_signal("click_piece", self)


func _set_type():
	var index = floor(rand_range(0, piece_types.size()))
	type = index


func _create_mock_sprite():
	
	var root = get_tree().get_root()
	var self_sprite = get_node("Sprite")
	
	var mock_sprite = Sprite.new()
	mock_sprite.scale = Vector2(0.5, 0.5)
	mock_sprite.texture = self_sprite.texture
	root.add_child(mock_sprite)
	mock_sprite.position = global_position
	return mock_sprite
	
func __dump__():
	return "i:(%s) t:(%s) l:%s p:%s s:%s d:%s" % [ id, type, board_location, board_position, is_selected, is_dirty]

	