
#	Copyright 2018, SpockerDotNet LLC

extends Node


signal score_changed(score)


var score 
var warp
var speed
var shields
var weapons
var power
var game_board
var root


func add_score(points):
	score += points
	emit_signal("score_changed", score)


func _init():
	print("Session:_init")
	score = 0
	warp = 0
	speed = 0
	shields = 0
	weapons = 0
	power = 0
	
	
	
