
#	Copyright 2018, SpockerDotNet LLC

extends Area2D


var sprite
var hold_position
var move_to
var ship_speed = 1
var tween

func _init():
	pass	
	
	
func _ready():
	print("ship ready")
	sprite = get_node("Sprite")
	hold_position = position
	tween = get_node("Tween")


func _on_IdleTimer_timeout():
	tween.stop_all()
	var x_offset = abs(rand_range(-50, 50))
	var y_offset = abs(rand_range(-50, 50))
	move_to = hold_position + Vector2(x_offset + (ship_speed * 10), y_offset)
	tween.interpolate_property(self, "position", position, move_to , 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	