extends "res://LudumDare/Game/Ship.gd"


var ticks = 0
export(int) var weapon_cooldown
var target

func _ready():
	print("pirate ship ready")
	var target = find_node("Player")
	print(target)
	ticks = floor(rand_range(-120,120))

func _process(delta):
	
	ticks += 1
	
	if ticks > weapon_cooldown:
		
		ticks = floor(rand_range(-120,120))
		
		var type = floor(rand_range(0, 2))
		
		if type == 0:
			print("shoot laser")
			
		if type >=0:
			print("shoot missle")
			var missle = load("res://LudumDare/Game/Missle.tscn").instance()
			missle.target = target
			get_node("/root").add_child(missle)
			missle.global_position = global_position			