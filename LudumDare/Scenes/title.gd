extends Control


var _next_scene


func _ready():
	get_tree().paused = false
	$Menu/Body/Buttons/New.grab_focus()
	for button in $Menu/Body/Buttons.get_children():
		button.connect("load_scene", self, "_on_load_scene")
	

func _on_load_scene(scene):
	print(scene)
	_next_scene = scene
	$FadeIn.show()
	$FadeIn.fade_in()


func _on_FadeIn_fade_completed():
	print(_next_scene)
	get_tree().change_scene(_next_scene)
