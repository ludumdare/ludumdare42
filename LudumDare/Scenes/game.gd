
#	Copyright 2018, SpockerDotNet LLC

extends Node

#	actual values of the ship systems
var ship_warp = 0
var ship_speed = 100
var ship_shield = 100
var ship_weapon = 25
var ship_power = 100
var ship_hull = 10

#	system event value
export(int) var warp_charge_ticks
export(int) var speed_cooldown_ticks
export(int) var shield_cooldown_ticks
export(int) var weapon_charge_ticks
export(int) var power_cooldown_ticks
export(int) var travel_distance_ticks

#	system event value
export(int) var warp_charge
export(int) var speed_cooldown
export(int) var shield_cooldown
export(int) var weapon_charge
export(int) var power_cooldown

#	number of ticks in process
var warp_ticks = 0
var speed_ticks = 0
var shield_ticks = 0
var weapon_ticks = 0
var power_ticks = 0
var game_ticks = 0
var travel_ticks = 0


var warp_location
var speed_location
var shield_location
var weapon_location
var power_location

enum game_states { GAME_STATE_READY, GAME_STATE_RESOLVE, GAME_STATE_SWAP, GAME_STATE_REWIND }
enum animation_states { ANIMATION_STATE_CLUSTERS, ANIMATION_STATE_PIECES, ANIMATION_STATE_SWAP, ANIMATION_STATE_REWIND }


var game_board
var game_state


var animation_state = 0
var animation_time = 0
var animation_total = 0.3


var current_move_piece

var sound_match
var sound_bonus
var sound_powerup
var sound_hit
var sound_laser
var sound_warp
var sound_warning
var sound_pirate
var sound_select


var player

func _set_hull(hull):
	ship_hull = hull
	if ship_hull <=0:
		ship_hull = 0
		print("dead")
		
	Signals.emit_signal(Signals.GAME_HULL_CHANGED, ship_hull)
	
	
func _set_warp(warp):
	ship_warp = warp
	if ship_warp > 100:
		ship_warp = 100
		print("win")
		
	Signals.emit_signal(Signals.GAME_WARP_CHANGED, ship_warp)
	
	
func _set_speed(speed):
	print("set speed")
	ship_speed = speed
	if ship_speed > 100:
		ship_speed = 100
		print("max speed")

	if ship_speed <= 10:
		sound_warning.play()
				
	if ship_speed <= 0:
		ship_speed = 0
		print("not moving - captured")
		_game_over()
		
	Signals.emit_signal(Signals.GAME_SPEED_CHANGED, ship_speed)
	
	
func _set_shield(shield):
	ship_shield = shield
	if ship_shield > 100:
		ship_shield = 100
		print("max_shields")
		
	if ship_shield <= 0:
		ship_shield = 0
		print("no shields")
		
	Signals.emit_signal(Signals.GAME_SHIELD_CHANGED, ship_shield)
	
	
func _set_weapon(weapon):
	ship_weapon = weapon
	if ship_weapon > 100:
		ship_weapon = 100
		print("max weapon")
		
	if ship_weapon <= 0:
		ship_weapon = 0
		print("no weapons")
		
	Signals.emit_signal(Signals.GAME_WEAPON_CHANGED, ship_weapon)
	
	
func _set_power(power):
	ship_power = power
	if ship_power > 100:
		ship_power = 100
		print("max powers")
		
	if ship_power <= 0:
		ship_power = 0
		print("no power - no specials")
		
	Signals.emit_signal(Signals.GAME_POWER_CHANGED, ship_power)
	
	
func _ready():
	print("game:_ready")
	game_board = get_node("GameBoard")
	player = find_node("Player")
	find_node("Pirate1").target = player
	find_node("Pirate2").target = player
	find_node("Pirate3").target = player
	Session.root = get_node(".")
	game_board.create()
	game_board.draw()
	_connect_to_pieces()
	game_state = GAME_STATE_READY
	warp_location = get_node("GuiShipSystems").warp_location.global_position
	speed_location = get_node("GuiShipSystems").speed_location.global_position
	shield_location = get_node("GuiShipSystems").shield_location.global_position
	weapon_location = get_node("GuiShipSystems").weapon_location.global_position
	power_location = get_node("GuiShipSystems").power_location.global_position
	sound_match = get_node("Audio/SoundMatch")
	sound_bonus = get_node("Audio/SoundBonus")
	sound_powerup = get_node("Audio/SoundPowerup")
	sound_hit = get_node("Audio/SoundHit")
	sound_laser = get_node("Audio/SoundLaser")
	sound_warp = get_node("Audio/SoundWarp")
	sound_warning = get_node("Audio/SoundWarning")
	sound_pirate = get_node("Audio/SoundPirateLaser")
	sound_select = get_node("Audio/SoundSelect")
	_set_hull(ship_hull)
	_set_warp(ship_warp)
	_set_speed(ship_speed)
	_set_shield(ship_shield)
	_set_weapon(ship_weapon)
	_set_power(ship_power)
	Signals.connect(Signals.LOAD_SCENE, self, "_on_load_scene")
	get_tree().paused = false
	var tween = get_node("Tween")
	var ships = get_node("Travel/Ships")
	tween.interpolate_property(ships, "position", ships.position, ships.position + Vector2(512,0), 3.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	Signals.connect(Signals.HIT_PLAYER, self, "_on_hit_player")
	_get_ready()
	
	
func _get_ready():
	var message = find_node("Message")
	message.text = "get ready!"
	message.rect_position = Vector2(512, 30)
	var tween = get_node("Tween")
	tween.interpolate_property(message, "rect_position", message.rect_position, message.rect_position + Vector2(0, 0), 2.0, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	tween.interpolate_property(message, "rect_position", message.rect_position, message.rect_position + Vector2(-1280, 0), 2.0, Tween.TRANS_QUINT, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	
	
func _input(event):
	
	if event.is_action("pause"):
		get_tree().paused = true
		_paused()
	
func _process(delta):
	
	if game_state == GAME_STATE_READY:
		game_ticks += 1
		warp_ticks += 1
		speed_ticks += 1
		shield_ticks += 1
		weapon_ticks += 1
		power_ticks += 1				
		travel_ticks += 1
		
	Signals.emit_signal(Signals.GAME_UPDATE_TRAVEL, travel_ticks, travel_distance_ticks)
	
	if travel_ticks > travel_distance_ticks:
		_you_win()
		
	if game_state == GAME_STATE_SWAP:
		pass	
		
	if warp_ticks > warp_charge_ticks:
		warp_ticks = 0
		_set_warp(ship_warp + warp_charge)
			
	if speed_ticks > speed_cooldown_ticks:
		speed_ticks = 0
		_set_speed(ship_speed - speed_cooldown)
		
	if shield_ticks > shield_cooldown_ticks:
		shield_ticks = 0
		_set_shield(ship_shield - shield_cooldown)
		
	if weapon_ticks > weapon_charge_ticks:
		weapon_ticks = 0
		_set_weapon(ship_weapon + weapon_charge)
		
	if power_ticks > power_cooldown_ticks:
		power_ticks = 0
		_set_power(ship_power - power_cooldown)
			
	var status = get_node("GuiBottom/Status")
	status.text = "%s : FPS(%02d) : %05d" % [ game_states.keys()[game_state], Engine.get_frames_per_second(), game_ticks ]
	
	
#	swap two game pieces
func _swap():
	
	#	simulate pieces swapping
	var tween1 = current_move_piece.tween_to_position(game_board.selected.global_position)
	var tween2 = game_board.selected.tween_to_position(current_move_piece.global_position)
	
#	game_board.selected.tween_to_position()
#	var tween = current_move_piece.tween_to_position()
	yield( tween1.tween, "tween_completed")
	tween1.tween.stop_all()
	tween1.sprite.queue_free()
	tween2.tween.stop_all()
	tween2.sprite.queue_free()
	print("swap_done")
	
	#	wait until completed
	
	#	swap the pieces
	game_board.swap(current_move_piece, game_board.selected)
	current_move_piece.draw()
	game_board.selected.draw()
	
	#	was it successful?
	
	var clusters = game_board.find_clusters()
	
	if clusters.size() > 0:
		print("success")
		current_move_piece.reset()
		game_board.selected.reset()
		current_move_piece = null
		game_board.selected = null
		_resolve_clusters()
		print("done")
	else:
		#	simulate reversing the swap		
		print("not success")
		tween1 = current_move_piece.tween_to_position(game_board.selected.global_position)
		tween2 = game_board.selected.tween_to_position(current_move_piece.global_position)
		yield( tween1.tween, "tween_completed")
		print("swap_rewind_done")
		tween1.tween.stop_all()
		tween1.sprite.queue_free()
		tween2.tween.stop_all()
		tween2.sprite.queue_free()
		current_move_piece.reset()
		game_board.selected.reset()
		#	and swap
		game_board.swap(current_move_piece, game_board.selected)
		#	reset
		current_move_piece = null
		game_board.selected = null
		
	game_state = GAME_STATE_READY
	game_board.draw()
		
func _game_over():
		get_tree().paused = true
		$GameOver.show()
		
func _you_win():
		get_tree().paused = true
		$YouWin.show()
		
func _paused():
		get_tree().paused = true
		$Pause.show()
		
			

func _connect_to_pieces():
	for piece in game_board.pieces.values():
		#print(piece.name)
		piece.connect("click_piece", self, "_on_click_piece")


func _on_click_piece(piece):
	
	#	not ready, return :)
	if game_state != GAME_STATE_READY:
		return
		
	sound_select.play()
		
	var swapped = false
	
	#	we already have a selected piece
	if game_board.selected:
		if game_board.selected.is_selected:
			if piece == game_board.selected:
				game_board.selected.is_selected = false
				return
			else:
				if game_board.can_swap(piece):
					# swap
					print("swap")
					swapped = true
					current_move_piece = piece
					game_state = GAME_STATE_SWAP
					_swap()
	
	#	nothing was swapped
	if !swapped:
		game_board.selected = piece
		game_board.selected.is_selected = true
		game_board.selected.draw()


func _on_load_scene(scene):
	print(scene)
	get_tree().change_scene(scene)
	
func _render():
	for piece in game_board.pieces.values():
		piece.render()


func _animate_cluster(cluster):
	print("game_board:_animate_clusters")
	
	var x_offset = 0
	var y_offset = 0
	for i in cluster.length:
		var key = Vector2(cluster.board_location.x + x_offset, cluster.board_location.y + y_offset)
		var piece = game_board.pieces[key]
		
		var to_pos = Vector2(0, 0)
		
		if cluster.type == 0:
			to_pos = warp_location
			_set_warp(ship_warp + 1 + (cluster.length - 3))
			
		if cluster.type == 1:
			to_pos = speed_location
			_set_speed(ship_speed + 1 + (cluster.length - 3))
			
		if cluster.type == 2:
			to_pos = shield_location
			_set_shield(ship_shield + 1 + (cluster.length - 3))
			
		if cluster.type == 3:
			to_pos = weapon_location
			_set_weapon(ship_weapon + 1 + (cluster.length - 3))
			
		if cluster.type == 4:
			to_pos = power_location
			_set_power(ship_power + 1 + (cluster.length - 3))
			
		piece.remove_piece(to_pos)
	
		sound_match.play()
		
		if cluster.horizontal:
			x_offset += 1
		else:
			y_offset += 1
	
	
func _resolve_clusters():
	print("game_board:_resolve_clusters")
	
	game_board.draw()
	
	var clusters = game_board.find_clusters()
	
	while clusters.size() > 0:

		for cluster in clusters:
			
			_animate_cluster(cluster)
			Session.add_score(cluster.length * 100)
				
			game_board.remove_clusters(clusters)
			
			var shifts = game_board.find_shifts()
			
			while shifts.size() > 0:
				
				for shift in shifts:
#					print("from:%s, to:%s" % [shift.from, shift.to])
					var from = game_board.pieces[shift.from]
					var to = game_board.pieces[shift.to]
#					var move = from.move_to(to.global_position)
					game_board.swap(game_board.pieces[shift.from], game_board.pieces[shift.to])
					from.draw()
#					move.tween.start()
#					yield(move.tween, "tween_completed")
##					print("animate_shift_done")
#					move.sprite.queue_free()
					to.draw()
			
				
				shifts = game_board.find_shifts()
			
	
			game_board.resolve_pieces()
	
			clusters = game_board.find_clusters()

#			print(game_board.__dump__("type"))
			
			game_board.draw()


func _on_hit_player(object, damage):
	if object == "missle":
		if ship_shield > 0:
			_set_shield(ship_shield - damage)
			player.show_shield()
		else:
			_set_hull(ship_hull - 1)			
		
	
func _on_Continue_pressed():
	get_tree().paused = false

