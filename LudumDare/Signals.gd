
#	Copyright 2018, SpockerDotNet LLC

extends Node

const GAME_HULL_CHANGED = "game_hull_changed"
const GAME_WARP_CHANGED = "game_warp_changed"
const GAME_SPEED_CHANGED = "game_speed_changed"
const GAME_SHIELD_CHANGED = "game_shield_changed"
const GAME_WEAPON_CHANGED = "game_weapon_changed"
const GAME_POWER_CHANGED = "game_power_changed"
const GAME_UPDATE_TRAVEL = "game_update_travel"
const LOAD_SCENE = "load_scene"
const HIT_PLAYER = "hit_player"


#	signals for changes in systems 
signal game_hull_changed(hull)
signal game_warp_changed(warp)
signal game_speed_changed(speed)
signal game_shield_changed(sheilds)
signal game_weapon_changed(weapons)
signal game_power_changed(power)
signal load_scene(scene)
signal hit_player(object, damage)
signal game_update_travel(travel)