extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var target = find_node("Player")
	print(target)
	var enemy = find_node("Pirate")
	print(enemy)
	enemy.target = target

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
