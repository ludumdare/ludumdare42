extends Node


var ticks = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):

	ticks += 1
	if ticks > 100:
		ticks = 0
		Signals.emit_signal(Signals.GAME_WARP_CHANGED, floor(rand_range(0,100)))
		Signals.emit_signal(Signals.GAME_SPEED_CHANGED, floor(rand_range(0,100)))
		Signals.emit_signal(Signals.GAME_SHIELD_CHANGED, floor(rand_range(0,100)))
		Signals.emit_signal(Signals.GAME_WEAPON_CHANGED, floor(rand_range(0,100)))
		Signals.emit_signal(Signals.GAME_POWER_CHANGED, floor(rand_range(0,100)))
				