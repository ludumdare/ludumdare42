extends Node

var tick = 0
var hull = 20

func _process(delta):
	
	tick += 1
	
	if tick > 100:
		tick = 0
		hull -= 1
		if hull > 0:
			Signals.emit_signal(Signals.GAME_HULL_CHANGED, hull)

