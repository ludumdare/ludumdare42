# Ludum Dare 42

Source code for my Ludum Dare 42 entry.

## Naming Conventions

### Folders
Folders should be structured to contain Nodes or Objects supporting the Nodes. All Folder names should be in PascalCase with the exception of the "addons" folder which is created by Godot for  plugins.

    res://
    |--addons
    |--Fonts
    |   |--TrueType
    |--Gui
    |--Scenes
    |--Scripts
    |--TileMaps
    |   |--Dungeon
    |--Game
    |   |--Player
    |   |--Enemy
    |   |--Items
    |   |   |--Default
    |   |   |--Chest
    |   |--Levels
    |   |   |--Level1
    |   |   |--Level2

### Nodes
All Nodes in a Scene should be in named in PascalCase. 

    TitleButton
    Player
    SpawnTimer
    GameTileMap

### Node Scenes
When a Node converted to a Scene it should also be Saved with the name in PascalCase. If the Scene is based on another Scene, the Base Scene should be in front of the Name itself.

    TitleButton_About
    TitleButton_Options
    Enemy_Ghost
    Item_Chest

### Scripts
Scripts should always be named in snake_case unless it is being used as a Class and will be inherited by another Script.

    bootstrap.gd
    autoload.gd
    some_script.gd
    MyClass.gd

Scripts attached to Nodes or Node Scenes should be named using the same words as the Node but in snake_case.

    title_button.gd --> attached to base TitleButton
    title_button_about.gd --> attached to TitleButton_About

### Script Structure
In most cases we have chosen to follow the [GDScript Style Guide](https://github.com/Calinou/godot-style-guide) with some additional style references from [PEP-8](https://www.python.org/dev/peps/pep-0008/) style guide for Python. 

#### Regions

- signals
- constants
- enums
- exports
- setget
- vars
- onready vars
- private functions
- public functions
- internal classes

#### Variables
All variables should follow the snake_case naming convention, event when declaring a new object from a class. Private members in a function should start with an underscore. Parameters in a function should never start with an underscore.

    extends Node

    var my_name
    var some_name

    func some_function(some_parameter):
        my_name = some_parameter
        var _name = my_name
        some_name = _name

#### Indentation
Use 4 spaces per indentation level. Indentation should use Spaces and NOT Tabs, this can be done in the Editor Settings under Text Editor > Indent > Type

    for x in range(10):
        print(x)

#### Blank Lines
Blank lines should be used sparingly to separate groups of related functions. Use blank lines in functions to indicate logical sections of code. No blank lines should be after a function declaration unless separating logical blocks of code, allow two blank lines should follow after the last line of code in a function.

    func my_cool_function():
        another_function()


    func another_function():

        do_something()
        print('hello')

        do_something_else()
        print('world')

#### Commenting
Commenting should be kept up to date when the code changes. They should be in complete sentences, and the first word should always be capitalized unless it is an identifier. Block Comments generally consist of multiple paragraphs in complete sentences with a period. Single line comments should contain an indent prior to adding the comment. Use inline comments sparingly.

    """
        Some very length comments using a full
        sentence structure ending in a period.
    """

Yes

        #   a single line comment

No

        # a single line comment


Yes

        var number_of_bullets = 0

No

        var nob = 0     # number of bullets

#### Documentation Strings
TBD

